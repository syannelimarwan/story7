from django.test import TestCase , Client
from django.urls import resolve
from .views import profile

# Create your tests here.
class Story7UnitTest(TestCase):
    def test_story7_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)
    
    def test_story7_using_profile_func(self):
    	found = resolve('/')
    	self.assertEqual(found.func, profile)
    
    def test_story7_using_profile_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'profile.html')



