$(document).ready(function(){

	$(function () {
        $("#accordion").accordion({
          collapsible: true,
          active: false,
          heightStyle: "content",
        });
    });

    $("input").click(() => {
        let body = document.querySelector('body')
        if (body.classList.contains('light')) {
            body.classList.remove('light');
            body.classList.add('dark');
            document.getElementById("theme").innerHTML = "LIGHT?";
        }
        else {
            body.classList.remove('dark');
            body.classList.add('light');
            document.getElementById("theme").innerHTML = "DARK?";
        }
    });
    

});

